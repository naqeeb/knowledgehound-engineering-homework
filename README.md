KnowledgeHound interview homework
=====================================

**Note**: This is a private repository. Do not share it with anyone and please work on this alone.

Please treat this assignment as if you're writing real production code. Engineer your code to meet your professional standards and include anything else you'd include if you were submitting this for work. And feel free to contact us if you have any questions.

The background
--------------
For market research purposes, companies frequently commission consumer studies, which are typically surveys. Surveys consist of a sequence of questions. For some questions, respondents may only choose one answer (think radio buttons), and for others more than one answer (think checkboxes).

Additionally, some questions may only be asked if the respondent had a specific answer to a previous question. For example, if a person answers "yes" to "Do you have pets?", then subsequently s/he may be shown the question "Do you own a dog?". It wouldn't make sense to ask him/her about dogs if s/he didn't own pets. Alternatively, s/he might be asked a multiple-answer question, such as "What kinds of pets do you have?", to which a valid answer would be "fish, dog". Subsequently, the survey may ask such a respondent "What kind of dog food do you (or rather your dog) prefer?".

See *simple_sample.csv* for sample survey data. The structure of the data file is intended to be straightforward, with each RespondentID representing a person who took the survey. Questions with multiple answers are comma delimited. 

The challenge
-------------

Based on the data, we want to determine which questions were shown to each respondent, by reverse-engineering the conditions that determine which respondents saw which questions in the survey. We'd like you to implement that functionality.

The Spec
--------

The *survey.py* file in this repository represents an existing, (if simple – and imperfect), codebase. Please modify it so that each question can report the question(s), if any, which determined whether or not it was shown to a respondent.

In particular, implement the method `get_conditionals()` on `Question`, which returns a list of dictionaries showing which conditions determined the visibility of that instance of `Question`.

Example of a working implementation processing *simple_sample.csv*:

    >>> import survey
    >>> dataset = survey.SurveyDataSet('simple_sample.csv')
    >>>
    >>> dataset.get_question("Do you have pets?").get_conditionals()
    []
    >>>
    >>> dataset.get_question("What kind of pets do you have?").get_conditionals()
    [{'where_response_equals': 'Yes', 'determined_by': <Question: Do you have pets?>}]
    >>>
    >>> dataset.get_question("Where do you shop for pet food?").get_conditionals()
    [{'where_response_equals': 'Yes', 'determined_by': <Question: Do you have pets?>}]
    >>>
    >>> dataset.get_question("Do you visit dog parks?").get_conditionals()
    [{'where_response_equals': 'Dog', 'determined_by': <Question: What kind of pets do you have?>}]

Guidelines
----------

### Functionality

- You need only check for equality "==" conditions. For example, if "Do you eat bacon?" was shown only to people whose favorite color was "red". You do not need to report inequality conditions, (*e.g.* "only shown to people whose favorite band was NOT nickelback). Nor must you check for complicated AND/OR conditions.
- Questions to which all respondents gave the same answer can't be a determiner. For example, if all respondents report "Human" to the question "Are you a human or a robot?", that question should never appear as a determiner of other questions.
- The column order in the CSV file represents the order in the survey – questions which occur later in the survey can't be used to determine whether questions before them were seen, (no time travel!).
- You can assume the CSV file is simple, valid and contiguous, (no tricky empty rows, extra commas, quotes).
- All question labels (the first row of the CSV) will be unique. No trickery from us there.

### Other

- Use Python, its standard library, and any dependencies you would include if this were code going to production.
- Performance is important. This should work relatively quickly on datasets with up to 2,000 questions and 50,000 respondents. Please limit performance optimizations to native Python. In our use case, a file is only parsed once, when a user uploads it; but, each question's possible conditionals will be accessed repeatedly by end users. However, you may ignore persistence, and assume this code runs in memory.
- Extensibility is important. In the real world, there are many subclasses of `Question`. For example, some questions are numeric and others allow people to "check all that apply".
- We hope that you can complete this on your own, but if the requirements aren't clear or something seems wrong, ask questions. That's what you would do at work.
- The code we're providing you isn't perfect. Sometimes refactoring is necessary. And thoughtful critiques are welcome.

Happy hacking!
--------------

– The KnowledgeHound Team
